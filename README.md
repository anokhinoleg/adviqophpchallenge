# Coding Challenge

## Up and run env
To run the app locally, run ```docker-compose up --build``` command in the project directory.
It will start the php server on [localhost](http://localhost:8000).
## Configure symfony
Copy configuration to the __.env.example__ file in the project directory
``` 
###> symfony/framework-bundle ###
APP_ENV=dev
APP_SECRET=aaa3eb0ff208ecb1866f123f8af0bff1
###< symfony/framework-bundle ###

###> docker environment ###
DATABASE_NAME=app_db
DATABASE_USER=root
DATABASE_ROOT_PASSWORD=test
###< docker environment ###

###> doctrine/doctrine-bundle ###
# Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# IMPORTANT: You MUST configure your server version, either here or in config/packages/doctrine.yaml
#
# DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
# DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7"
DATABASE_URL=mysql://${DATABASE_USER}:${DATABASE_ROOT_PASSWORD}@app_db:3306/${DATABASE_NAME}?serverVersion=mariadb-10.5.8
###> doctrine/doctrine-bundle ###
```
## Setup app
Run ```docker exec -ti test_app bash``` to jump into php docker container.
Install dependencies with ```composer install``` and run migrations ```bin/console doctrine:migrations:migrate```
## Using api
One now should be able to visit [api sandbox page](http:/localhost:8000/api/doc)
## Staging
Application also available on [heroku](https://adviqo-php-challenge.herokuapp.com/api/doc)
## Tests
Since I haven't written tests before, I skipped this step due to the lack of time. But you are free to commit to my project. :)