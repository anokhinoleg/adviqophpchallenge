<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220529094347 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE advisor (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(80) NOT NULL, description TEXT DEFAULT NULL, availability TINYINT(1) DEFAULT \'0\', price_per_minute NUMERIC(15, 2) NOT NULL, profile_image LONGBLOB DEFAULT NULL, languages SET(\'afr\', \'hye\', \'ces\', \'eng\', \'fra\', \'ita\', \'jpn\', \'ukr\', \'uzb\', \'cym\', \'yid \') DEFAULT \'eng\' NOT NULL COMMENT \'(DC2Type:simple_array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE advisor');
    }
}
