<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\Migrations\Exception\MigrationException;

class Version20220602094347 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('INSERT INTO user (email, roles, password) VALUES (\'php_challenge@adviqo.com\', \'[]\', \'$argon2id$v=19$m=65536,t=4,p=1$aXNw8bKy0GcKweKWxh0obA$+Khjkm9siEeYW4aZrKAU7pwQG0tEjKl1BUtyxAJigPI\')');
    }
}