<?php declare(strict_types=1);

namespace App\Api;

interface FilterParamModifierInterface
{
    /**
     * @param mixed $value
     *
     * @return mixed
     */
    public function modify(mixed $value) : mixed;

    /**
     * @return string
     */
    public function getName() : string;
}