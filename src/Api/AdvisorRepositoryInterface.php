<?php declare(strict_types=1);

namespace App\Api;

use App\Entity\Advisor;

interface AdvisorRepositoryInterface
{
    /**
     * @param array $criteria
     *
     * @return Advisor[]
     */
    public function getList(array $criteria) : array;

    /**
     * @param int $id
     *
     * @return Advisor|null
     */
    public function getById(int $id) : ?Advisor;

    /**
     * @param Advisor $advisor
     *
     * @return int
     */
    public function save(Advisor $advisor) : int;

    /**
     * @param Advisor $advisor
     *
     * @return void
     */
    public function delete(Advisor $advisor) : void;
}