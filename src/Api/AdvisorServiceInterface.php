<?php declare(strict_types=1);

namespace App\Api;

use App\Entity\Advisor;
use App\Exception\AdvisorAlreadyExistsException;
use App\Exception\AdvisorConstraintViolationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

interface AdvisorServiceInterface
{
    /**
     * @param array|null $filterParams
     *
     * @return Advisor[]
     */
    public function getListByFilters(?array $filterParams) : array;

    /**
     * @param int $id
     *
     * @return Advisor
     * @throws NotFoundHttpException
     */
    public function getById(int $id) : Advisor;

    /**
     * @param mixed $data
     *
     * @return Advisor
     * @throws AdvisorAlreadyExistsException
     * @throws AdvisorConstraintViolationException
     */
    public function create(mixed $data) : Advisor;

    /**
     * @param mixed $data
     * @param int   $id
     *
     * @return Advisor
     * @throws AdvisorConstraintViolationException
     */
    public function update(mixed $data, int $id) : Advisor;

    /**
     * @param bool|string|null $image
     * @param int              $id
     *
     * @return void
     * @throws AdvisorConstraintViolationException
     */
    public function saveImage(bool|string|null $image, int $id) : void;

    /**
     * @param int $id
     *
     * @return void
     */
    public function deleteById(int $id) : void;
}