<?php declare(strict_types=1);

namespace App\DBAL\Types;

class LanguageSetType
{
    /**
     * @var array|string[] - ISO 639-3
     */
    static public array $values = [
        'afr',
        'hye',
        'ces',
        'eng',
        'fra',
        'ita',
        'jpn',
        'ukr',
        'uzb',
        'cym',
        'yid '
    ];
}