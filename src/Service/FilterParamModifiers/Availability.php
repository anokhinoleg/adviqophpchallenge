<?php declare(strict_types=1);

namespace App\Service\FilterParamModifiers;

use App\Api\FilterParamModifierInterface;

class Availability implements FilterParamModifierInterface
{
    private const PARAM_NAME_TO_MODIFY = 'availability';

    private const MAP_AVAILABILITY = [
        'false' => false,
        'true' => true
    ];

    /**
     * @param mixed $value
     *
     * @return mixed
     */
    public function modify(mixed $value) : mixed
    {
        return self::MAP_AVAILABILITY[$value];
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return self::PARAM_NAME_TO_MODIFY;
    }
}