<?php declare(strict_types=1);

namespace App\Service\FilterParamModifiers;

use App\Api\FilterParamModifierInterface;

class Languages implements FilterParamModifierInterface
{
    private const PARAM_NAME_TO_MODIFY = 'languages';

    /**
     * @param mixed $value
     *
     * @return mixed
     */
    public function modify(mixed $value): mixed
    {
        return explode(' ', $value);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::PARAM_NAME_TO_MODIFY;
    }
}