<?php declare(strict_types=1);

namespace App\Service;

use App\Api\AdvisorInterface;
use Doctrine\Persistence\Mapping\ReflectionService;

class AdvisorFactory
{
    /**
     * @var string
     */
    protected string $instanceName;

    /**
     * @var ReflectionService
     */
    private ReflectionService $reflectionService;

    /**
     * @param ReflectionService $reflectionService
     * @param string            $advisorInstanceName
     */
    public function __construct(ReflectionService $reflectionService, string $advisorInstanceName)
    {
        $this->reflectionService = $reflectionService;
        $this->instanceName = $advisorInstanceName;
    }

    /**
     * @param array        $data
     * @param AdvisorInterface|null $existedAdvisor
     *
     * @return AdvisorInterface
     */
    public function create(array $data, ?AdvisorInterface $existedAdvisor = null): AdvisorInterface
    {
        $advisor = $existedAdvisor ?? new $this->instanceName();

        foreach ($data as $propertyName => $value) {
            $methodName = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $propertyName)));
            if ($this->reflectionService->hasPublicMethod($this->instanceName, $methodName)) {
                $advisor->$methodName($value);
            }
        }

        return $advisor;
    }
}