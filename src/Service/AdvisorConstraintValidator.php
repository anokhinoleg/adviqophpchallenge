<?php declare(strict_types=1);

namespace App\Service;

use App\Exception\AdvisorConstraintViolationException;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class AdvisorConstraintValidator
{
    private const MAX_IMAGE_SIZE = 1024 * 1024 * 2; //~2MB
    private const ALLOWED_IMAGE_EXTENSIONS = ['png', 'jpg', 'jpeg'];

    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param mixed $data
     * @param bool  $allowMissingFields
     *
     * @throws AdvisorConstraintViolationException
     */
    public function validate(mixed $data, bool $allowMissingFields): void
    {
        $constraint = new Assert\Collection([
            'fields' => [
                'name' => new Assert\Required([
                    new Assert\NotNull(),
                    new Assert\NotBlank(),
                    new Assert\Length(['min' => 2]),
                    new Assert\Type('string')
                ]),
                'description' => new Assert\Optional([]),
                'availability' => new Assert\Optional([
                    new Assert\Type('boolean')
                ]),
                'price_per_minute' => new Assert\Required([
                    new Assert\NotNull(),
                    new Assert\NotBlank(),
                    new Assert\Length(['min' => 2]),
                    new Assert\Type('string'),
                    new Assert\Regex('/^[0-9]*\.[0-9][0-9]$/'),
                    new Assert\Positive()
                ]),
                'languages' => new Assert\Optional([
                    new Assert\Type('array'),
                    new Assert\Count(['min' => 1])
                ])
            ],
            'allowMissingFields' => $allowMissingFields
        ]);
        $violations = $this->validator->validate($data, $constraint);

        if ($violations->count()) {
            $violationsMessage = [];
            /** @var ConstraintViolationInterface $violation */
            foreach ($violations as $violation) {
                $violationsMessage[$violation->getPropertyPath()] = $violation->getMessage();
            }

            throw new AdvisorConstraintViolationException(json_encode($violationsMessage));
        }
    }

    /**
     * @param bool|string|null $image
     *
     * @throws AdvisorConstraintViolationException
     */
    public function validateImage(bool|string|null $image): void
    {
        $violationsMessage = [];
        $finfo = new \finfo(FILEINFO_MIME_TYPE);
        $mimeType = $finfo->buffer($image);
        $mime = substr($mimeType, strpos($mimeType, '/') + 1);

        if (strlen($image) > self::MAX_IMAGE_SIZE) {
            $violationsMessage['size'] = 'Legal file size limit 2MB';
        }

        if (!in_array($mime, self::ALLOWED_IMAGE_EXTENSIONS)) {
            $violationsMessage['mime'] = 'Legal file extensions are: png, jpg, jpeg';
        }

        if (count($violationsMessage)) {
            throw new AdvisorConstraintViolationException(json_encode($violationsMessage));
        }
    }
}