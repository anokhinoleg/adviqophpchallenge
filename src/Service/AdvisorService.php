<?php declare(strict_types=1);

namespace App\Service;

use App\Api\AdvisorRepositoryInterface;
use App\Api\AdvisorServiceInterface;
use App\Api\FilterParamModifierInterface;
use App\Entity\Advisor;
use App\Exception\AdvisorAlreadyExistsException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdvisorService implements AdvisorServiceInterface
{
    /**
     * @var AdvisorRepositoryInterface
     */
    private AdvisorRepositoryInterface $advisorRepository;

    /**
     * @var AdvisorFactory
     */
    private AdvisorFactory $advisorFactory;

    /**
     * @var FilterParamModifierInterface[]
     */
    private array $filterParamModifiers;

    /**
     * @var AdvisorConstraintValidator
     */
    private AdvisorConstraintValidator $validator;

    /**
     * @param AdvisorRepositoryInterface $advisorRepository
     * @param AdvisorFactory             $advisorFactory
     * @param AdvisorConstraintValidator $validator
     * @param array                      $filterParamModifiers
     */
    public function __construct(
        AdvisorRepositoryInterface $advisorRepository,
        AdvisorFactory $advisorFactory,
        AdvisorConstraintValidator $validator,
        array $filterParamModifiers = []
    ) {
        $this->advisorRepository = $advisorRepository;
        $this->advisorFactory = $advisorFactory;
        $this->filterParamModifiers = $filterParamModifiers;
        $this->validator = $validator;
    }

    /**
     * {@inheritDoc}
     */
    public function getListByFilters(?array $filterParams) : array
    {
        foreach ($this->filterParamModifiers as $filterParamModifier) {
            if (isset($filterParams[$filterParamModifier->getName()])) {
                $filterParams[$filterParamModifier->getName()] =
                    $filterParamModifier->modify(
                        $filterParams[$filterParamModifier->getName()]
                    );
            }
        }

        return $this->advisorRepository->getList($filterParams);
    }

    /**
     * {@inheritDoc}
     */
    public function getById(int $id) : Advisor
    {
        return $this->advisorRepository->getById($id) ??
            throw new NotFoundHttpException(sprintf('Advisor with id %d not found', $id));
    }

    /**
     * {@inheritDoc}
     */
    public function create(mixed $data) : Advisor
    {
        $this->validator->validate($data, false);
        $advisor = $this->advisorFactory->create($data);

        if ($advisor->getId() && $this->advisorRepository->getById($advisor->getId())) {
            throw new AdvisorAlreadyExistsException(sprintf('Resource with id %d already exists', $advisor->getId()));
        }

        $id = $this->advisorRepository->save($advisor);

        return $this->advisorRepository->getById($id);
    }

    /**
     * {@inheritDoc}
     */
    public function update(mixed $data, int $id) : Advisor
    {
        $this->validator->validate($data, true);
        $advisor = $this->getById($id);
        $advisor = $this->advisorFactory->create($data, $advisor);
        $this->advisorRepository->save($advisor);

        return $advisor;
    }

    /**
     * {@inheritDoc}
     */
    public function saveImage(bool|string|null $image, int $id) : void
    {
        $this->validator->validateImage($image);
        $advisor = $this->getById($id);
        $advisor->setProfileImage($image);
        $this->advisorRepository->save($advisor);
    }

    /**
     * {@inheritDoc}
     */
    public function deleteById(int $id) : void
    {
        $advisor = $this->getById($id);
        $this->advisorRepository->delete($advisor);
    }
}