<?php

namespace App\Entity;

use App\Api\AdvisorInterface;
use App\Repository\AdvisorRepository;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;

/**
 * @OA\Schema()
 * @ORM\Entity(repositoryClass=AdvisorRepository::class)
 */
class Advisor implements AdvisorInterface
{
    /**
     * @Groups({"show_advisor", "list_advisor"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @Groups({"show_advisor", "list_advisor"})
     * @OA\Property(type="string", property="name", nullable=false)
     * @ORM\Column(type="string", length=80)
     */
    public string $name;

    /**
     * @Groups({"show_advisor", "list_advisor"})
     * @OA\Property(type="string", property="description", nullable=true)
     * @ORM\Column(type="text", nullable=true)
     */
    public ?string $description;

    /**
     * @Groups({"show_advisor", "list_advisor"})
     * @OA\Property(type="boolean", property="availability", nullable=true, readOnly=true, default=false)
     * @ORM\Column(type="boolean", nullable=false, options={"default"= 0})
     */
    public bool $availability = false;

    /**
     * @Groups({"show_advisor", "list_advisor"})
     * @OA\Property(type="string", property="price_per_minute", nullable=false)
     * @ORM\Column(name="price_per_minute", type="decimal", precision=15, scale=2)
     */
    public string $pricePerMinute;

    /**
     * @Ignore()
     * @OA\Property(type="string", property="profile_image", nullable=true, writeOnly=true, format="binary")
     * @ORM\Column(name="profile_image", type="blob", nullable=true)
     */
    public $profileImage;

    /**
     * @Groups({"show_advisor", "list_advisor"})
     * @var string[]
     * @OA\Property(
     *     type="object",
     *     example={"afr", "hye", "ces", "eng", "fra", "ita", "jpn", "ukr", "uzb", "cym", "yid"},
     *     property="languages"
     * )
     * @ORM\Column(type="simple_array", nullable=false, options={"default": "eng"})
     */
    public $languages;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAvailability(): bool
    {
        return $this->availability;
    }

    public function setAvailability(?bool $availability): self
    {
        $this->availability = (bool)$availability;

        return $this;
    }

    public function getPricePerMinute(): string
    {
        return $this->pricePerMinute;
    }

    public function setPricePerMinute(string $pricePerMinute): self
    {
        $this->pricePerMinute = $pricePerMinute;

        return $this;
    }

    public function getLanguages(): array
    {
        return $this->languages;
    }

    public function setLanguages(array $languages): self
    {
        $this->languages = $languages;

        return $this;
    }

    public function getProfileImage()
    {
        return $this->profileImage;
    }

    public function setProfileImage($profileImage): self
    {
        $this->profileImage = $profileImage;

        return $this;
    }
}
