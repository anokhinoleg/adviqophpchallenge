<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController
{
    #[Route('/index/index')]
    public function index(): JsonResponse
    {
        return new JsonResponse(['name' => 'Oleh']);
    }

    /**
     * @Route("/index/{name}")
     */
    public function name(string $name): Response
    {
        return new Response('<html><body>Lucky number: ' . $name . '</body></html>');
    }
}