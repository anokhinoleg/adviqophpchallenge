<?php declare(strict_types=1);

namespace App\Controller\Api;

use App\Api\AdvisorServiceInterface;
use App\Exception\AdvisorAlreadyExistsException;
use App\Exception\AdvisorConstraintViolationException;
use App\Repository\UserRepository;
use Firebase\JWT\JWT;
use Nelmio\ApiDocBundle\Annotation\Model;
use App\Entity\Advisor;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class AdvisorController
{
    /**
     * @var AdvisorServiceInterface
     */
    private AdvisorServiceInterface $advisorService;

    /**
     * @var string
     */
    private string $jwtSecret;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @param AdvisorServiceInterface $advisorService
     * @param SerializerInterface     $serializer
     * @param string                  $jwtSecret
     */
    public function __construct(
        AdvisorServiceInterface $advisorService,
        SerializerInterface $serializer,
        string $jwtSecret
    ) {
        $this->advisorService = $advisorService;
        $this->jwtSecret = $jwtSecret;
        $this->serializer = $serializer;
    }

    /**
     *
     * @Route("/api/auth", methods={"GET"})
     * @OA\Get(
     *     tags={"Authentication"},
     *     path="/api/advisor/auth",
     *     summary="Authentication",
     *     description="Authenticate user"
     * )
     * @OA\Tag(name="Authentication")
     * @OA\Parameter(
     *     name="email",
     *     in="query",
     *     description="Email for authentication",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *         example="email@example.com"
     *     ),
     *     style="form"
     * )
     * @OA\Parameter(
     *     name="password",
     *     in="query",
     *     description="Password for authentication",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *         example="Admin123"
     *     ),
     *     style="form"
     * )
     */
    public function authenticate(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $encoder): JsonResponse
    {
        $user = $userRepository->findOneBy([
            'email' => $request->get('email'),
        ]);

        if (!$user || !$encoder->isPasswordValid($user, $request->get('password'))) {
            return new JsonResponse([
                'message' => 'Email r password is wrong.',
            ]);
        }

        $payload = [
            "user" => $user->getUsername(),
            "exp"  => (new \DateTime())->modify("+30 minutes")->getTimestamp(),
        ];

        $jwt = JWT::encode($payload, $this->jwtSecret, 'HS256');

        return new JsonResponse([
            'message' => 'Success!',
            'token' => sprintf('Bearer %s', $jwt),
        ]);
    }

    /**
     *
     * @Route("/api/advisor/list", methods={"GET"})
     * @OA\Get(
     *     path="/api/advisor/list",
     *     summary="Find all advisors filtered by availability",
     *     description="Returns the list of advisors",
     *     operationId="getList",
     *     security={{ "bearerAuth": {} }},
     * )
     * @OA\Parameter(
     *     name="availability",
     *     in="query",
     *     description="Availability that needs to be considered for filter",
     *     required=false,
     *     @OA\Schema(
     *         type="boolean"
     *     ),
     *     style="form"
     * )
     * @OA\Parameter(
     *     name="name",
     *     in="query",
     *     description="Name that needs to be considered for filter",
     *     required=false,
     *     @OA\Schema(
     *         type="string"
     *     ),
     *     style="form"
     * )
     * @OA\Parameter(
     *     name="languages",
     *     in="query",
     *     description="Languages that need to be considered for filter",
     *     required=false,
     *     @OA\Schema(
     *         type="object",
     *         example={"afr", "hye", "ces", "eng", "fra", "ita", "jpn", "ukr", "uzb", "cym", "yid"}
     *     ),
     *     style="spaceDelimited"
     * )
     * @OA\Parameter(
     *     name="sort_order",
     *     in="query",
     *     description="Sort by price",
     *     required=false,
     *     @OA\Schema(
     *         enum={"ASC", "DESC"}
     *     ),
     *     style="form"
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns the list of advisors",
     *     @OA\JsonContent(
     *         type="array",
     *         @OA\Items(ref=@Model(type=Advisor::class))
     *     )
     * )
     * @OA\Response(
     *     response=500,
     *     description="Server internal error",
     *     @OA\JsonContent(
     *         type="array",
     *         @OA\Items(type="string"),
     *         example={"message": "Response message"}
     *     )
     * )
     * @OA\Tag(name="Advisor")
     */
    public function getList(Request $request): JsonResponse
    {
        try {
            $filterParams = $request->query->all();
            return new JsonResponse(
                $this->serializer->serialize(
                    $this->advisorService->getListByFilters($filterParams),
                    'json',
                    [AbstractNormalizer::IGNORED_ATTRIBUTES => ['profileImage']]
                ),
                Response::HTTP_OK,
                json: true
            );
        } catch (\Exception $e) {
            return new JsonResponse(
                ['message' => $e->getMessage()],
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    /**
     * @Route("/api/advisor/{id}", methods={"GET"})
     * @OA\Get(
     *     path="/api/advisor/{id}",
     *     summary="Find advisor by ID",
     *     description="Returns a single advisor",
     *     operationId="getAdvisorById",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of advisor to return",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successfully retrieved advisor",
     *         @OA\JsonContent(ref=@Model(type=Advisor::class))
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid ID supplier",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(type="string"),
     *             example={"message": "Response message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Advisor not found",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(type="string"),
     *             example={"message": "Response message"}
     *         )
     *     ),
     *     security={{ "bearerAuth": {} }},
     * )
     * @OA\Tag(name="Advisor")
     */
    public function getAdvisorById(int $id): JsonResponse
    {
        try {
            return new JsonResponse(
                $this->serializer->serialize(
                    $this->advisorService->getById($id),
                    'json',
                    [AbstractNormalizer::IGNORED_ATTRIBUTES => ['profileImage']]
                ),
                Response::HTTP_OK,
                json: true
            );
        } catch (NotFoundHttpException $e) {
            return new JsonResponse(
                ['message' => $e->getMessage()],
                $e->getStatusCode()
            );
        }
    }

    /**
     * @Route("/api/advisor/new", methods={"POST"})
     * @OA\Post(
     *     path="/api/advisor/new",
     *     operationId="addAdvisor",
     *     summary="Add a new advisor",
     *     description="Adds a new advisor record and returns it",
     *     @OA\RequestBody(
     *         description="Advisor object that needs to be added",
     *         required=true,
     *         @OA\JsonContent(
     *             ref=@Model(type=Advisor::class),
     *             @OA\Examples(
     *                 example="New advisor json template",
     *                 summary="",
     *                 value={"name": "username", "description": "advisor description", "price_per_minute": "0.50", "languages": {"eng"}}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Advisor created successfully",
     *         @OA\JsonContent(ref=@Model(type=Advisor::class))
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid input",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(type="string"),
     *             example={"message": "Response message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="Advisor already exists conflict",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(type="array", @OA\Items(type="string")),
     *             example={"messages": "Response message"}
     *         )
     *     ),
     *     security={{ "bearerAuth": {} }},
     * )
     * @OA\Tag(name="Advisor")
     * @throws \JsonException
     */
    public function addAdvisor(Request $request): JsonResponse
    {
        $data = json_decode(
            $request->getContent(), true, 512, JSON_THROW_ON_ERROR
        );

        try {
            return new JsonResponse(
                $this->serializer->serialize(
                    $this->advisorService->create($data),
                    'json',
                    [AbstractNormalizer::IGNORED_ATTRIBUTES => ['profileImage']]
                ),
                Response::HTTP_CREATED,
                json: true
            );
        } catch (AdvisorAlreadyExistsException $e) {
            return new JsonResponse(
                ['message' => $e->getMessage()],
                Response::HTTP_CONFLICT
            );
        } catch (AdvisorConstraintViolationException $e) {
            return new JsonResponse(
                ['messages' => json_decode($e->getMessage(), true)],
                Response::HTTP_BAD_REQUEST
            );
        } catch (\Exception $e) {
            return new JsonResponse(
                ['message' => $e->getMessage()],
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    /**
     * @param Request $request
     * @param int     $id
     *
     * @return JsonResponse
     * @throws \JsonException
     * @Route("/api/advisor/{id}/update", methods={"PATCH"})
     * @OA\Patch(
     *     path="/api/advisor/{id}/update",
     *     operationId="updateAdvisorById",
     *     summary="Update an existing advisor",
     *     description="Updates advisor record by given id",
     *     @OA\RequestBody(
     *         description="Advisor object that needs to be updated",
     *         required=true,
     *         @OA\JsonContent(
     *             ref=@Model(type=Advisor::class),
     *             @OA\Examples(
     *                 example="Advisor update json template",
     *                 summary="",
     *                 value={"name": "username", "description": "advisor description", "availability": true, "price_per_minute": "0.50", "languages": {"eng"}}
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Advisor updated successfully",
     *         @OA\JsonContent(ref=@Model(type=Advisor::class),)
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid input",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(type="string"),
     *             example={"message": "Response message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Advisor not found",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(type="string"),
     *             example={"message": "Response message"}
     *         )
     *     ),
     *     security={{ "bearerAuth": {} }},
     * )
     * @OA\Tag(name="Advisor")
     */
    public function updateAdvisorById(Request $request, int $id): JsonResponse
    {
        $data = json_decode(
            $request->getContent(), true, 512, JSON_THROW_ON_ERROR
        );

        try {
            return new JsonResponse(
                $this->serializer->serialize(
                    $this->advisorService->update($data, $id),
                    'json',
                    [AbstractNormalizer::IGNORED_ATTRIBUTES => ['profileImage']]
                ),
                Response::HTTP_OK,
                json: true
            );
        } catch (NotFoundHttpException $e) {
            return new JsonResponse(
                ['message' => $e->getMessage()],
                Response::HTTP_NOT_FOUND
            );
        } catch (AdvisorConstraintViolationException $e) {
            return new JsonResponse(
                ['messages' => json_decode($e->getMessage(), true)],
                Response::HTTP_BAD_REQUEST
            );
        } catch (\Exception $e) {
            return new JsonResponse(
                ['message' => $e->getMessage()],
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    /**
     * @Route("/api/advisor/{id}/upload-image", methods={"POST"})
     * @OA\Post(
     *     path="/api/advisor/{id}/upload-image",
     *     summary="Uploads an image",
     *     operationId="uploadFile",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of advisor for which the image uploaded",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Image uploaded successfully",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(type="string"),
     *             example={"message": "Response message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid input",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(type="string"),
     *             example={"message": "Response message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Advisor not found",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(type="string"),
     *             example={"message": "Response message"}
     *         )
     *     ),
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         description="Upload images request body",
     *         @OA\MediaType(
     *             mediaType="application/octet-stream",
     *             @OA\Schema(
     *                 type="string",
     *                 format="binary"
     *             )
     *         )
     *     )
     * )
     * @OA\Tag(name="Advisor")
     */
    public function uploadImage(Request $request, int $id): JsonResponse
    {
        try {
            $this->advisorService->saveImage($request->getContent(), $id);

            return new JsonResponse(
                $this->serializer->serialize(
                    ['message' => sprintf('Profile image for Advisor with id %d successfully uploaded', $id)],
                    'json'
                ),
                Response::HTTP_CREATED,
                json: true
            );
        } catch (NotFoundHttpException $e) {
            return new JsonResponse(
                ['message' => $e->getMessage()],
                Response::HTTP_NOT_FOUND
            );
        } catch (AdvisorConstraintViolationException $e) {
            return new JsonResponse(
                ['messages' => json_decode($e->getMessage(), true)],
                Response::HTTP_BAD_REQUEST
            );
        } catch (\Exception $e) {
            return new JsonResponse([
                ['message' => $e->getMessage()],
                Response::HTTP_BAD_REQUEST
            ]);
        }
    }

    /**
     * @Route("/api/advisor/{id}/delete", methods={"DELETE"})
     * @OA\Delete(
     *     path="/api/advisor/{id}/delete",
     *     summary="Delete advisor by ID",
     *     description="Deletes advisor record with the given id",
     *     operationId="deleteAdvisorById",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of advisor to delete",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(type="string"),
     *             example={"message": "Response message"}
     *          )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid input",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(type="string"),
     *             example={"message": "Response message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Advisor not found",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(type="string"),
     *             example={"message": "Response message"}
     *         )
     *     ),
     *     security={{ "bearerAuth": {} }},
     * )
     * @OA\Tag(name="Advisor")
     */
    public function deleteAdvisorById(int $id): JsonResponse
    {
        try {
            $this->advisorService->deleteById($id);
            return new JsonResponse(
                $this->serializer->serialize(
                    ['message' => sprintf('Advisor with id %d successfully deleted', $id)],
                        'json',
                    ),
                Response::HTTP_OK,
                json: true
            );
        } catch (NotFoundHttpException $e) {
            return new JsonResponse(
                ['message' => $e->getMessage()],
                Response::HTTP_NOT_FOUND
            );
        }
    }
}