<?php

namespace App\Repository;

use App\Api\AdvisorRepositoryInterface;
use App\Entity\Advisor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Advisor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Advisor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Advisor[]    findAll()
 * @method Advisor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdvisorRepository extends ServiceEntityRepository implements AdvisorRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Advisor::class);
    }

    /**
     * @return Advisor[] Returns an array of Advisor objects
     */
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Advisor
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * {@inheritDoc}
     */
    public function getList(array $criteria) : array
    {
        $queryBuilder = $this->createQueryBuilder('a');

        foreach ($criteria as $filterParam => $filterBy) {
            $filterMethodName = 'filterBy' . ucfirst($filterParam);
            if (method_exists($this, $filterMethodName)) {
                $this->$filterMethodName($queryBuilder, $filterBy);
            }
        }

        $queryBuilder->orderBy('a.pricePerMinute', $criteria['sort_order'] ?? 'ASC');

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * {@inheritDoc}
     */
    public function getById(int $id) : ?Advisor
    {
        return $this->find($id);
    }

    /**
     * {@inheritDoc}
     */
    public function save(Advisor $advisor) : int
    {
        if (!$advisor->getId()) {
            $this->_em->persist($advisor);
        }

        $this->_em->flush();

        return $advisor->getId();
    }


    /**
     * {@inheritDoc}
     */
    public function delete(Advisor $advisor) : void
    {
        $this->_em->remove($advisor);
        $this->_em->flush();
    }

    private function filterByAvailability(QueryBuilder $queryBuilder, mixed $availability): void
    {
        $queryBuilder->andWhere('a.availability = :availability')
            ->setParameter('availability', $availability);
    }

    private function filterByName(QueryBuilder $queryBuilder, mixed $name): void
    {
        $queryBuilder->andWhere('a.name = :name')
            ->setParameter('name', $name);
    }

    private function filterByLanguages(QueryBuilder $queryBuilder, mixed $languages) : void
    {
        $length = count($languages);
        $whereClauseExp = '';

        foreach ($languages as $index => $language) {
            if ((int)$index !== $length - 1) {
                $whereClauseExp .= $queryBuilder->expr()->like('a.languages', '\'%' . $language . '%\'') . ' OR ';
                continue;
            }

            $whereClauseExp .= $queryBuilder->expr()->like('a.languages',  '\'%' . $language . '%\'');
        }

        if ($whereClauseExp !== '') {
            $queryBuilder->andWhere('(' . $whereClauseExp . ')');
        }
    }
}
